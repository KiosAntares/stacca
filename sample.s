# global data  #
.data
format_int: .asciz "%d\n"
stack: .fill 1024

.text
    .global main
print_int:    
  push %rbx
  lea  format_int(%rip), %rdi
  xor %eax, %eax
  call printf
  pop %rbx
  ret
 
push_stack:
   movq %rax, stack(%r15)
   add $8, %r15
   ret

pop_stack:
    sub $8, %r15
    movq stack(%r15), %rax   
    ret

main:
    xor %r15, %r15    

    mov $12, %rax
    call push_stack
    mov $13, %rax
    call push_stack

    xor %rax, %rax

    call pop_stack
    mov %rax, %rsi
    call print_int
    call pop_stack
    mov %rax, %rsi
    call print_int

    push $5
    pop %rsi
    call print_int
    

    ret

# 5 2 + 3 * print
