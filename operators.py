"""Provides the internals code"""

POP_RAX = """
 sub $8, %r15
 movq stack(%r15), %rax
""".strip()

POP_RDX = """
 sub $8, %r15
 movq stack(%r15), %rdx
""".strip()

POP_RSI = """
 sub $8, %r15
 movq stack(%r15), %rsi
""".strip()

PUSH_RAX = """
   movq %rax, stack(%r15)
   add $8, %r15
""".strip()

PUSH_RDX = """
   movq %rdx, stack(%r15)
   add $8, %r15
""".strip()

PUSH_RBX = """
   movq %rbx, stack(%r15)
   add $8, %r15
""".strip()

operator_code = {
    "+": f"""
  {POP_RAX}
  {POP_RDX}
  add %rax, %rdx
  {PUSH_RDX}
    """,
    "-": f"""
  {POP_RAX}
  {POP_RDX}
  sub %rax, %rdx
  {PUSH_RDX}
    """,
    "inc": f"""
  {POP_RAX}
  inc %rax
  {PUSH_RAX}
    """,
    "dec": f"""
  {POP_RAX}
  dec %rax
  {PUSH_RAX}
    """,
    "*": f"""
  {POP_RAX}
  {POP_RDX}
  imul %rax, %rdx
  {PUSH_RDX}
    """,
    "=": f"""
  {POP_RAX}
  {POP_RDX}
  xor %rbx, %rbx
  cmp %rax, %rdx
  setz %bl
  {PUSH_RBX}
    """,
    "<": f"""
  {POP_RAX}
  {POP_RDX}
  xor %rbx, %rbx
  cmp %rax, %rdx
  setl %bl
  {PUSH_RBX}
    """,
    ">": f"""
  {POP_RAX}
  {POP_RDX}
  xor %rbx, %rbx
  cmp %rax, %rdx
  setg %bl
  {PUSH_RBX}
    """,
    "or": f"""
  {POP_RAX}
  {POP_RDX}
  or %rax, %rdx
  {PUSH_RDX}
    """,
    "and": f"""
  {POP_RAX}
  {POP_RDX}
  and %rax, %rdx
  {PUSH_RDX}
    """,
    "swap": f"""
  {POP_RAX}
  {POP_RDX}
  {PUSH_RAX}
  {PUSH_RDX}
    """,
    "dup": f"""
  {POP_RAX}
  {PUSH_RAX}
  {PUSH_RAX}
    """,
    "slot": """
  sub $8, %r15
  movq stack(%r15), %r14
    """,
    "unslot": """
  movq %r14, stack(%r15)
  add $8, %r15
    """,
    "drop": """
  sub $8, %r15
    """,
    "print": f"""
  {POP_RSI}
  call print_int
  """,
    "printLn": f"""
  {POP_RSI}
  call print_int_ln
  """,
}
