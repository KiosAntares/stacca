#!/bin/env python3
"""The STACCA compiler"""

# pylint: disable=too-many-branches
# pylint: disable=too-few-public-methods

from typing import Union
import sys
from operators import operator_code, POP_RAX
from snippets import ENTRY, HALT


def tokenize(string: str) -> list[str]:  # garbage but idc operator
    """Tokenize on space"""
    return string.split()


# half of this is useless, gotta figure out string and float operations
def parse(token: str) -> Union[int, float, str]:
    """Parses a single token to appropriate repr"""
    try:
        return int(token)
    except ValueError:
        try:
            return float(token)
        except ValueError:
            return token.replace("'", "")


# ugly ass thing
class Label:
    """Utilities for label"""

    __int = 0
    IF = 1
    IF_ELSE = 2
    WHILE = 3
    DEF = 4

    @classmethod
    def label(cls) -> int:
        """Returns new sequential label id"""
        temp = Label.__int
        Label.__int += 1
        return temp


def operator(token: str) -> str:
    """Returns code for operator"""
    return f"# {token}\n" + operator_code[token] + "\n"


def push(token: int) -> str:
    """Returns code to push an immediate"""
    return (
        f"# push {token} \n"
        + f"  mov ${token}, %rax \n"
        + "  movq %rax, stack(%r15) \n"
        + "  add $8, %r15 \n"
    )


def compile_stacca(code: list) -> str:
    """Compiles program to at&t amd64 assembly"""
    out = f"#PROGRAM: {' '.join(code)}"
    out += ENTRY
    # structure is (label_id, label_type)
    jump_stack = []

    for token in code:
        if token in operator_code:
            out += operator(token)

        elif token == "def":
            #  put jump point and then label
            label_id, label_type = Label.label(), Label.DEF
            jump_stack.append((label_id, label_type))
            out += f"  jmp L_{label_id}_E\n"

        elif token == "if":
            # add a jump segment in stack
            label_id, label_type = Label.label(), Label.IF
            jump_stack.append((label_id, label_type))

        elif token == "while":
            # add a jump segment in stack
            label_id, label_type = Label.label(), Label.WHILE
            jump_stack.append((label_id, label_type))
            # setup label for loop
            out += f"L_{label_id}:\n"

        elif token == "else":
            # switches label type to IF_ELSE, with a different end behaviour
            label_id, label_type = jump_stack.pop()
            jump_stack.append((label_id, Label.IF_ELSE))
            # true block ended, jump to exit
            out += f"  jmp L_{label_id}_EE\n\n"
            # label for false(else) block (equivalent to exit of normal if)
            out += f"L_{label_id}_E: # ELSE\n"

        elif token == "do":
            label_id, label_type = jump_stack[-1]
            # if true, doesn't jump
            # if false (test 0) jump to exit/else block
            out += f"  {POP_RAX}\n"
            out += "  test %rax, %rax\n"
            out += f"  jz L_{label_id}_E \n"

        elif token == "end":
            label_id, label_type = jump_stack.pop()
            if label_type == Label.WHILE:
                # loop
                out += f"  jmp L_{label_id}\n\n"
            if label_type == Label.DEF:
                out += "  ret\n\n"
            if label_type != Label.IF_ELSE:
                # exit label of normal if/while
                out += f"L_{label_id}_E: # IF/WHILE/RET \n"
            else:
                # exit label of IF_ELSE
                out += f"L_{label_id}_EE: #ELSE \n"

        else:
            tok = parse(token)
            if isinstance(tok, str):
                if jump_stack:
                    label_id, label_type = jump_stack[-1]
                    if label_type == Label.DEF:
                        out += f"{tok}:\n"
                else:
                    out += f"  call {tok}\n"
            else:
                out += push(token)  # yes.

    out += HALT
    return out


def main():
    """Main"""
    file_in = sys.argv[1]
    lines = ""
    with open(file_in, "r", encoding="utf-8") as input_file:
        for line in input_file:
            if not line.strip().startswith("#"):
                lines += line

    output = compile_stacca(tokenize(lines))
    print(output)


if __name__ == "__main__":
    main()
