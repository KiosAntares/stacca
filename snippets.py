"""Provides fundamental snippets for compilation"""

# sets up entry and provides internals
# backs up stack pointer to clean stack at end of execution
ENTRY = """
.text
  .global main
print_int_ln:    
  push %rbx
  lea  format_int_ln(%rip), %rdi
  xor %eax, %eax          
  call printf
  pop %rbx
  ret

print_int:    
  push %rbx
  lea  format_int(%rip), %rdi
  xor %eax, %eax          
  call printf
  pop %rbx
  ret

main:
  xor %r15, %r15
"""

# drop everything that remains in stack to avoid segfault
# original stack pointer was saved at beginnimg, so now pop until we back there
HALT = """
    ret

.data
  format_int_ln: .asciz "%d\\n"
  format_int: .asciz "%d"
  stack: .fill 640000
"""
